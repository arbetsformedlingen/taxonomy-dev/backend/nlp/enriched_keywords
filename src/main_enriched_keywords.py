import common
import json
import os, sys
sys.path.insert(0, os.path.abspath("../data"))
url_tax = "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql?query=query%20MyQuery%20%7B%0A%20%20concepts%20%20%7B%0A%20%20%20%20id%0A%20%20%20%20type%0A%20%20%20%20preferred_label%0A%20%20%20%20alternative_labels%0A%20%20%20%20hidden_labels%0A%20%20%7D%0A%7D%0A"

def get_type_lists():
    read_sort_occupation = common.read_txt_file("../data/sort_occupation.txt")
    read_sort_location = common.read_txt_file("../data/sort_location.txt")
    read_sort_skill = common.read_txt_file("../data/sort_skill.txt")

    return read_sort_occupation, read_sort_location, read_sort_skill

def get_tax_type_lists():
    tax = json.loads(common.get_taxonomy_concepts(url_tax))
    concepts = tax["data"]["concepts"]

    occupation_tax_list = [x for x in concepts if x["type"] in ["occupation-name", "keyword"]]
    location_tax_list = [x for x in concepts if x["type"] in ["region", "municipality"]]
    skill_tax_list = [x for x in concepts if x["type"] in ["skill"]] 

    return occupation_tax_list, location_tax_list, skill_tax_list

def main():
   occupation_list, location_list, skill_list = get_type_lists()
   occupation_tax_list, location_tax_list, skill_tax_list = get_tax_type_lists()

   common.get_connected_concepts_ods('occupation', occupation_tax_list, occupation_list)
   common.get_connected_concepts_ods('location', location_tax_list, location_list)
   common.get_connected_concepts_ods('skill', skill_tax_list, skill_list)

if __name__ == '__main__':
    main()
    print("connected_concepts_ods data done!")

