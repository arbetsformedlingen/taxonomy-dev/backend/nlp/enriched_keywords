import common
import json
import os, sys
sys.path.insert(0, os.path.abspath("../data"))

tax_link = "https://data.jobtechdev.se/taxonomy/version/latest/query/all-concepts/all-concepts.json"
synonymdic_competence_link = "https://jobad-enrichments-api.jobtechdev.se/synonymdictionary?type=COMPETENCE&spelling=CORRECTLY_SPELLED"
synonymdic_occupation_link = "https://jobad-enrichments-api.jobtechdev.se/synonymdictionary?type=OCCUPATION&spelling=CORRECTLY_SPELLED"
synonymdic_trait_link = "https://jobad-enrichments-api.jobtechdev.se/synonymdictionary?type=TRAIT&spelling=CORRECTLY_SPELLED"

def get_synonymdic():
    synonymdic_competence = common.get_all_concepts_synonymdic(synonymdic_competence_link)
    synonymdic_occupation = common.get_all_concepts_synonymdic(synonymdic_occupation_link)
    synonymdic_trait = common.get_all_concepts_synonymdic(synonymdic_trait_link)

    return synonymdic_competence, synonymdic_occupation, synonymdic_trait

def get_taxonomy_concepts():
    tax = json.loads(common.get_concepts_url(tax_link))
    data = tax["data"]["concepts"]
    all_concepts= common.get_lowercase_labels(data)

    return all_concepts

def main():
   synonymdic_competence, synonymdic_occupation, synonymdic_trait = get_synonymdic()
   print("synonymdic_competence_size: ", len(synonymdic_competence))
   print("synonymdic_occupation_size: ", len(synonymdic_occupation))
   print("synonymdic_trait_size: ", len(synonymdic_trait))
   
   all_concepts_tax = get_taxonomy_concepts()

   non_concepts_competence = common.get_non_concepts_in_tax(all_concepts_tax, synonymdic_competence)
   print("non_concepts_competence_size: ", len(non_concepts_competence)) 
   common.get_csv_file("competence", non_concepts_competence)

   non_concepts_occupation = common.get_non_concepts_in_tax(all_concepts_tax, synonymdic_occupation)
   print("non_concepts_occupation_size: ", len(non_concepts_occupation))
   common.get_csv_file("occupation", non_concepts_occupation)

   non_concepts_trait = common.get_non_concepts_in_tax(all_concepts_tax, synonymdic_trait)
   print("non_concepts_trait_size: ", len(non_concepts_trait))
   common.get_csv_file("trait", non_concepts_trait)


if __name__ == '__main__':
    main()
    print("Done!")