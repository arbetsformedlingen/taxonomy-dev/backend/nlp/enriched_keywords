from sentence_transformers import SentenceTransformer
from sklearn.neighbors import NearestNeighbors
from collections.abc import Iterable
from urllib.request import urlopen
from functools import lru_cache
import pandas as pd
import pyexcel_ods
import numpy as np
import json
import time
import re

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")

def get_csv_file(category, data):
    df = pd.DataFrame(data= {'Concepts not in Taxonomy': data})
    df.to_csv(f"../data/{category}_concepts.csv", index=False)

def get_json_file(filename):
    with open(filename, encoding="utf8") as f:
        return json.load(f)

def get_all_concepts_synonymdic(file_name):
    file_json = json.loads(get_concepts_url(file_name))
    concepts = file_json["items"]
    #all_concepts = [concept["concept"].lower() for concept in concepts] 
    #all_concepts = list(set(all_concepts))   
    #print("all_concepts_size: ", len(all_concepts))   
    return list({concept["concept"].lower() for concept in concepts})             

@lru_cache(360)
def get_concepts_url(url_tax):
    with urlopen(url_tax) as response:
        body = response.read()
    return body

def get_non_concepts_in_tax(tax_concepts, synonym_concepts):
    result = []
    for concept in synonym_concepts:
        if not (concept in tax_concepts):
            result.append(concept)
    result = list(set(result))
    return result

def get_ods_data(file_name='', col1='', col2='', col3=''):
    connected_concepts = pd.read_excel(file_name,
                                       usecols=[col1, col2, col3])
    connected_concepts = [value for value in connected_concepts.values]
    return connected_concepts

def filter_tax(item_list, tax_labels):
    if item_list not in tax_labels:
        return item_list

def remove_parentheses(item):
    return re.sub('[()]', '', item).split(", ")[0]

def get_labels(concept):
    return [label for labels in [concept["alternative_labels"], concept["hidden_labels"],
                                 [concept["preferred_label"]]] for label in labels]

def get_lowercase_labels(tax_list):
    #tax_labels = [get_labels(concept) for concept in tax_list]
    #tax_labels = [label.lower() for label in flatten(tax_labels)]
    #tax_labels = list(set(tax_labels))
    tax_labels = list({label.lower()
                   for concept in tax_list
                   for label in get_labels(concept)})
    print("all_concepts_size_tax: ", len(tax_labels))   
    return tax_labels

def concept_ids_dic(tax_labels, tax_list):
    dic_id = {}
    label_set = set(tax_labels)
    for concept in tax_list:
        for label in get_lowercase_labels([concept]):
            if label in label_set:
                dic_id[label] = concept["id"]
    return dic_id

def encode_vectors(labels_list):
    progress = ProgressReporter(len(labels_list))
    dst = []
    for label in labels_list:
        progress.end_of_iteration_message("Encoding...")
        dst.append(normalize(model.encode(label)))
    return dst

def nearest_neighbors(tax_labels, sort_list):
    vectors = encode_vectors(tax_labels)
    neighbors = NearestNeighbors(n_neighbors=20).fit(vectors)
    distances, indices = neighbors.kneighbors(encode_vectors(sort_list))
    print("distance shape is {:s}, indices shape is {:s}".format(str(distances.shape), str(indices.shape)))
    return indices

def connected_concepts_dic(sort_list, tax_labels, indices, concept_ids):
    dictionary = {}
    for i, occupation in enumerate(sort_list):
        indices_id = {}
        indices_list = [tax_labels[j] for j in indices[i, :]]
        for index in indices_list:
            indices_id[index] = concept_ids[index]
        dictionary[occupation] = indices_id
    return dictionary

def render_table(category, out_filename_ods, connected_concepts):
    dst = [[category, "Concept Id", "Connected Concept"]]
    for item in connected_concepts.keys():
        for key, value in connected_concepts[item].items():
            dst.append([item, value, key])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})

def get_connected_concepts_ods(category, tax_list, sort_list):
    tax_labels = get_lowercase_labels(tax_list)
    concept_ids = concept_ids_dic(tax_labels, tax_list)
    indices = nearest_neighbors(tax_labels, sort_list)
    connected_concepts = connected_concepts_dic(sort_list, tax_labels, indices, concept_ids)
    render_table(category, '../data/'+ category + '_connected_concepts' + '.ods', connected_concepts)

def read_txt_file(path):
    with open(path, "r") as f:
         file_list = [remove_parentheses(item) for item in f]
    return file_list

def normalize(x):
    return x / np.linalg.norm(x)

def get_taxonomy_concepts(url_tax):
    with urlopen(url_tax) as response:
        body = response.read()
    return body

def flatten(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

def compute_time_estimate(progress, elapsed_time):
    base = {"elapsed_time": elapsed_time,
            "progress": progress}
    if progress <= 0:
        return base
    time_per_progress = elapsed_time/progress
    remaining_progress = 1.0 - progress
    remaining_time = remaining_progress*time_per_progress
    return {**base,
            "time_per_progress": time_per_progress,
            "total_time": time_per_progress,
            "remaining_progress": remaining_progress,
            "remaining_time": remaining_time}

time_segmentation_seconds_breakdown = ["weeks", 7, "days", 24, "hours", 60, "minutes", 60, "seconds"]

def segment_time(t, segmentation=time_segmentation_seconds_breakdown):
    seg = list(reversed(segmentation))
    parts = []
    x = t
    while True:
        n = len(seg)
        unit = seg[0]
        if n == 1:
            parts.append((x, unit))
            break;
        else:
            d = seg[1]
            seg = seg[2:]
            xi = int(round(x))
            y = xi % d
            x = xi // d
            if 0 < y or (len(parts) == 0 and x == 0):
                parts.append((y, unit))
        if x == 0:
            break
    return parts

def format_time_segmentation(time_parts):
    n = min(2, len(time_parts))
    return ", ".join(map(lambda p: "{:d} {:s}".format(p[0], p[1]), reversed(time_parts[-n:])))

def format_seconds(seconds):
    return format_time_segmentation(segment_time(seconds))

def format_time_estimate(est):
    s = "Progress: {:d} %".format(int(round(100*est["progress"])))

    def timeinfo(k, lab):
        if k in est:
            return "\n{:s}: {:s}".format(lab, format_seconds(est[k]))
        return ""
    return s + timeinfo("total_time", "Total") + timeinfo("elapsed_time", "Elapsed") + timeinfo("remaining_time", "Remaining")

class ProgressReporter:
    def __init__(self, total_iterations, completed_iterations = 0, elapsed = 0):
        self.start = time.time() - elapsed
        self.total_iterations = total_iterations
        self.completed_iterations = completed_iterations
        self.rate_limiter = rate_limiter(1)

    def set_start(self, start):
        self.start = start

    def end_of_iteration(self):
        self.completed_iterations += 1

    def time_estimate(self):
        return compute_time_estimate(self.completed_iterations/self.total_iterations, time.time() - self.start)

    def elapsed(self):
        return time.time() - self.start

    def progress_report(self):
        return "Completed {:d} of {:d} iterations\n".format(
            self.completed_iterations, self.total_iterations) + format_time_estimate(self.time_estimate())

    def end_of_iteration_message(self, msg):
        self.end_of_iteration()
        if self.rate_limiter():
            print(msg)
            print(self.progress_report() + "\n")

    def to_data(self):
        return {"elapsed": self.elapsed(),
                "total_iterations": self.total_iterations,
                "completed_iterations": self.completed_iterations}

def rate_limiter(period=1):
    last = None

    def f():
        nonlocal last
        t = time.time()
        if (last is None) or (period + last) < t:
            last = t
            return True
        return False
    return f
