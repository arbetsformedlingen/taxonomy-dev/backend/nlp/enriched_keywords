import json
import common
from urllib.request import urlopen
from functools import lru_cache
import os, sys
sys.path.insert(0, os.path.abspath("../data"))

url = "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql?query=query%20MyQuery%20%7B%0A%20%20concepts%20%20%7B%0A%20%20%20%20id%0A%20%20%20%20type%0A%20%20%20%20preferred_label%0A%20%20%20%20alternative_labels%0A%20%20%20%20hidden_labels%0A%20%20%7D%0A%7D%0A"

@lru_cache(360)
def get_url_src(url):
    return urlopen(url).read()

def get_tax_labels():
    tax = json.loads(get_url_src(url))
    print(get_url_src.cache_info())
    concepts = tax["data"]["concepts"]
    alternative_labels = [list(map(lambda x: x.lower(), concept["alternative_labels"])) for concept in concepts]
    hidden_labels = [list(map(lambda x: x.lower(), concept["hidden_labels"])) for concept in concepts]
    preferred_labels = [concept["preferred_label"].lower() for concept in concepts]
    tax_concepts = [alternative_labels, hidden_labels, preferred_labels]
    return list(common.flatten(tax_concepts))

def get_enriched_keywords():
    tax_labels = get_tax_labels()
    with open('../data/2021-20220202-1303.jsonl', encoding="utf8") as json_file:
        json_list = list(json_file)

    enriched_occupation_list = []
    enriched_skill_list = []
    enriched_trait_list = []
    enriched_location_list = []
    enriched_compound_list = []

    for json_str in json_list:
        result = json.loads(json_str)
        if 'enriched' in result['keywords']:
            enriched = result['keywords']['enriched']
            enriched_occupation_list.extend(enriched['occupation'])
            enriched_skill_list.extend(enriched['skill'])
            enriched_trait_list.extend(enriched['trait'])
            enriched_location_list.extend(enriched['location'])
            enriched_compound_list.extend(enriched['compound'])

    occupation_notin_tax = filter(lambda item_list: common.filter_tax(item_list, tax_labels), enriched_occupation_list)
    skill_notin_tax = filter(lambda item_list: common.filter_tax(item_list, tax_labels), enriched_skill_list)
    trait_notin_tax = filter(lambda item_list: common.filter_tax(item_list, tax_labels), enriched_trait_list)
    location_notin_tax = filter(lambda item_list: common.filter_tax(item_list, tax_labels), enriched_location_list)
    compound_notin_tax = filter(lambda item_list: common.filter_tax(item_list, tax_labels), enriched_compound_list)

    enriched_keywords = {"occupation": occupation_notin_tax,
                         "skill": skill_notin_tax,
                         "trait": trait_notin_tax,
                         "location": location_notin_tax,
                         "compound": compound_notin_tax}
    return enriched_keywords

def get_dictionary_sort_keywords():
    enriched_keywords = get_enriched_keywords()
    dictionary_sort_keywords = {}
    for (category, keyword) in enriched_keywords.items():
        dictionary = {}
        dictionary_sort = []
        for item in keyword:
            if item in dictionary:
                dictionary[item] += 1
            else:
                dictionary[item] = 1

        dictionary_sort = sorted(dictionary.items(), key=
        lambda kv: (kv[1], kv[0]), reverse=True)
        dictionary_sort_keywords[category] = dictionary_sort
    return dictionary_sort_keywords

def main():
    dictionary_sort_keywords = get_dictionary_sort_keywords()
    for (category, dictionary_keyword) in dictionary_sort_keywords.items():
        with open("../data/sort_" + category + ".txt", "w") as f:
            print(*dictionary_keyword, sep='\n', file=f)

if __name__ == '__main__':
    main()
    print(".txt data done!")
