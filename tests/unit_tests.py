import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../src"))
import common
import main_enriched_keywords

class MyTestCase(unittest.TestCase):
    def test_get_type_lists(self):
        occupation_list, location_list, skill_list = main_enriched_keywords.get_type_lists()

        self.assertTrue("'vakt'" in occupation_list)
        self.assertTrue("'grängesberg'" in location_list)
        self.assertTrue("'c#.net'" in skill_list)

        self.assertFalse('Sara' in occupation_list)
        self.assertFalse('Sara' in location_list)
        self.assertFalse('Sara' in skill_list)

    def test_get_tax_type_lists(self):
        occupation_tax_list, location_tax_list, skill_tax_list = main_enriched_keywords.get_tax_type_lists()
        occupation_tax_labels = [common.get_labels(concept) for concept in occupation_tax_list]
        location_tax_labels = [common.get_labels(concept) for concept in location_tax_list]
        skill_tax_labels = [common.get_labels(concept) for concept in skill_tax_list]

        self.assertTrue(['Kriminalvårdsutbildning'] in occupation_tax_labels)
        self.assertTrue(['Zonguldak'] in location_tax_labels)
        self.assertTrue(['Miljödata'] in skill_tax_labels)

        self.assertFalse(['Sara'] in occupation_tax_labels)
        self.assertFalse(['Sara'] in location_tax_labels)
        self.assertFalse(['Sara'] in skill_tax_labels)

    def test_location_connected_concepts_ods(self):
        location_connected_concepts = common.get_ods_data(file_name='../data/location_connected_concepts.ods', 
                                                          col1='location', col2='Concept Id', col3='Connected Concept' )[:40]
        col1 = [list_data[0] for list_data in location_connected_concepts]
        col2 = [list_data[2] for list_data in location_connected_concepts]
        sample_col1 = "'västra götaland'"
        sample_col2 = 'västra götalands län'
        self.assertEqual(col1[0], sample_col1)
        self.assertEqual(col2[0], sample_col2)
    
    def test_occupation_connected_concepts_ods(self):
        occupation_connected_concepts = common.get_ods_data(file_name='../data/occupation_connected_concepts.ods', 
                                                          col1='occupation', col2='Concept Id', col3='Connected Concept' )[:40]
        col1 = [list_data[0] for list_data in occupation_connected_concepts]
        col2 = [list_data[2] for list_data in occupation_connected_concepts]
        sample_col1 = "'försäljare'"
        sample_col2 = 'företagssäljare'
        self.assertEqual(col1[0], sample_col1)
        self.assertEqual(col2[0], sample_col2)

    def test_skill_connected_concepts_ods(self):
        occupation_connected_concepts = common.get_ods_data(file_name='../data/skill_connected_concepts.ods', 
                                                          col1='skill', col2='Concept Id', col3='Connected Concept' )[:40]
        col1 = [list_data[0] for list_data in occupation_connected_concepts]
        col2 = [list_data[2] for list_data in occupation_connected_concepts]
        sample_col1 = "'försäljning'"
        sample_col2 = 'sales'
        self.assertEqual(col1[0], sample_col1)
        self.assertEqual(col2[0], sample_col2)

if __name__ == '__main__':
    unittest.main()

