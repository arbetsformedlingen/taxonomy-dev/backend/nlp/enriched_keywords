# Enriched Keywords

This repository contains list the most relevant enriched keywords to be added in the taxonomy. We used Sentence-BERT model to find them.

## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  `pip install - r requirements.txt`

## Expected result:

First run `sort_keywords.py` to get `sort_compound.txt`, `sort_location.txt`, `sort_occupation.txt`, `sort_skill.txt` and `sort_trait.txt`.

Then run `main.py` to get `location_connected_concepts.ods`, `occupation_connected_concepts.ods` and `skill_connected_concepts.ods`.
